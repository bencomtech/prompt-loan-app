import { useEffect } from "react";

import "antd/dist/antd.css";
import "../styles/vars.css";
import "../styles/globals.css";

const liffId = "1656526433-XE5dm2Yd";

function MyApp({ Component, pageProps }) {
  useEffect(async () => {
    const liff = (await import("@line/liff")).default;

    try {
      await liff.init({ liffId });
    } catch (error) {
      console.error("liff init error", error.message);
    }
  });

  return <Component {...pageProps} />;
}

export default MyApp;
